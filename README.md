mlplasmids documentation
================

<p align="center">
<img src="data/logo.png" alt="logo_package" width="600">
</p>

# Introduction

**mlplasmids** consists of binary classifiers to predict contigs either
as plasmid-derived or chromosome-derived. It currently classifies
short-read contigs as chromosomes and plasmids for *Enterococcus
faecium*, *Enterococcus faecalis*, *Klebsiella pneumoniae*,
*Acinetobacter baumannii* and *Enterococcus lactis*. The provided
classifiers are **Support Vector Machines** which were previously
optimized and selected versus other existing machine-learning techniques
(Logistic regression, Random Forest…, see also [Reference]()). The
classifiers use pentamer frequencies (n = 1,024) to infer whether a
particular contig is plasmid- or chromosome- derived.

<img src="data/input_output.png" width="500px" style="display: block; margin: auto;" />

# Requirements

-   R (tested in version 3.3.3)
-   [devtools](https://cran.r-project.org/web/packages/devtools/index.html)

# Dependencies

-   [kernlab](https://cran.r-project.org/web/packages/kernlab/index.html)
-   [Biostrings](https://bioconductor.org/packages/release/bioc/html/Biostrings.html)
-   [mlr](https://cran.r-project.org/web/packages/kernlab/index.html)
-   [seqinr](https://cran.rstudio.com/web/packages/seqinr/index.html)

Devtools will search for the above packages in your library path. If
some package(s) are not found, devtools will download and automatically
compile required packages.

# Installation with conda

``` bash
ls .condarc
 - conda-forge
 - bioconda
 - defaults
```

Clone the git repository

``` bash
git clone https://gitlab.com/mmb-umcu/mlplasmids.git
```

Create conda environment using the \*.yml file provided

``` bash
conda env create -f envs/mlplasmids.yml
```

Activate this new conda environment

``` bash
conda activate mlplasmids
```

Test the pipeline with the test data (’abaumannii_example.fasta). The
output will be in examples/abaumannii_prediction_example.tab

``` bash
Rscript scripts/run_mlplasmids.R data/abaumannii_example.fasta examples/abaumanni_prediction_example.tab 0.7 'Acinetobacter baumannii'
```

# Installation with R

You can install the R package using devtools, run the following in your
R console

``` r
install.packages("devtools")
devtools::install_git("https://gitlab.com/sirarredondo/mlplasmids")
```

``` r
library(mlplasmids)
```

## Shiny app implementation

**mlplasmids** has also been implemented as a user-friendly **Shiny
app.**

-   [Link to Shiny app.](https://sarredondo.shinyapps.io/mlplasmids/) -
    Version 2.1.0

<img src="data/shiny_app.png" width="800px" style="display: block; margin: auto;" />

# Usage

## Simple usage

-   You only have to specify the **path** pointing to your **fasta
    file** with the contig sequences. If the name does not contain
    relative or full path then the name of the file will be relative to
    current working directory.

-   The **value returned** by the function is a **dataframe** reporting
    predicted plasmid-derived contigs.

``` r
# Change the following object with the system location of your input file
my_path <- ('data/GCA_000250945.1_ASM25094v1_genomic.fna.gz')
example_prediction <- plasmid_classification(path_input_file = my_path, species = 'Enterococcus faecium')
```

``` r
head(example_prediction)
```

    ##   Prob_Chromosome Prob_Plasmid Prediction
    ## 2      0.02518783    0.9748122    Plasmid
    ## 3      0.06219903    0.9378010    Plasmid
    ## 4      0.03024578    0.9697542    Plasmid
    ##                                                                     Contig_name
    ## 2 CP003352.1 Enterococcus faecium Aus0004 plasmid AUS0004_p1, complete sequence
    ## 3 CP003353.1 Enterococcus faecium Aus0004 plasmid AUS0004_p2, complete sequence
    ## 4 CP003354.1 Enterococcus faecium Aus0004 plasmid AUS0004_p3, complete sequence
    ##   Contig_length
    ## 2         56520
    ## 3          3847
    ## 4          4119

### Species model

We have developed Support Vector Machine (SVM) models for predicting
plasmid- derived sequences for *Enterococcus faecium*, *Enterococcus
faecalis*, *Klebsiella pneumoniae*, *Escherichia coli*, *Acinetobacter
baumannii* and *Enterococcus lactis*.

-   The argument **species** is used to define which model to use.

``` r
example_prediction <- plasmid_classification(path_input_file = my_path, species = "Enterococcus faecium")
```

## Output explanation

| Prob. Chromosome | Prob.Plasmid | Prediction | Contig Name | Contig length |
|:----------------:|:------------:|:----------:|:-----------:|:-------------:|
|       0.92       |     0.08     | Chromosome |  Contig_12  |     22544     |
|       0.18       |     0.82     |  Plasmid   |  Contig_33  |     7144      |

-   *Prob_Chromosome*: **Posterior probability** for a particular object
    (contig) of belonging to the **chromosome class**.
-   *Prob_Plasmid*: **Posterior probability** for a particular object
    (contig) of belonging to the **plasmid class**.
-   *Prediction*: **Class** (Plasmid or Chromosome) **assigned** to a
    particular object (contig)
-   *Contig_name*: Name of the contig (parsed from the header)
-   *Contig_length*: Length of the contig

## Setting the probabilities threshold

By default the classifier assigns each object(contig) to the
class(plasmid or chromosome) with a highest posterior probability. We
can actually change the threshold (0.5) and e.g. only report
plasmid-predicted contigs with a posterior probability higher than 0.7
using the argument **prob_threshold**.

``` r
df_prob_prediction <- plasmid_classification(path_input_file = my_path, prob_threshold = 0.7, species = "Enterococcus faecium")
```

``` r
head(df_prob_prediction)
```

    ##   Prob_Chromosome Prob_Plasmid Prediction
    ## 2      0.02518783    0.9748122    Plasmid
    ## 3      0.06219903    0.9378010    Plasmid
    ## 4      0.03024578    0.9697542    Plasmid
    ##                                                                     Contig_name
    ## 2 CP003352.1 Enterococcus faecium Aus0004 plasmid AUS0004_p1, complete sequence
    ## 3 CP003353.1 Enterococcus faecium Aus0004 plasmid AUS0004_p2, complete sequence
    ## 4 CP003354.1 Enterococcus faecium Aus0004 plasmid AUS0004_p3, complete sequence
    ##   Contig_length
    ## 2         56520
    ## 3          3847
    ## 4          4119

## Reporting chromosome-predicted contigs

We can actually report contigs classified as chromosome-derived using
the argument **full_output**.

``` r
df_full_prediction <- plasmid_classification(path_input_file = my_path, full_output = TRUE, species = "Enterococcus faecium")
```

``` r
head(df_full_prediction)
```

    ##   Prob_Chromosome Prob_Plasmid Prediction
    ## 1      0.98887311   0.01112689 Chromosome
    ## 2      0.02518783   0.97481217    Plasmid
    ## 3      0.06219903   0.93780097    Plasmid
    ## 4      0.03024578   0.96975422    Plasmid
    ##                                                                     Contig_name
    ## 1                      CP003351.1 Enterococcus faecium Aus0004, complete genome
    ## 2 CP003352.1 Enterococcus faecium Aus0004 plasmid AUS0004_p1, complete sequence
    ## 3 CP003353.1 Enterococcus faecium Aus0004 plasmid AUS0004_p2, complete sequence
    ## 4 CP003354.1 Enterococcus faecium Aus0004 plasmid AUS0004_p3, complete sequence
    ##   Contig_length
    ## 1       2955294
    ## 2         56520
    ## 3          3847
    ## 4          4119

## Minimum contig length

By default we have defined a contig lenght threshold of 1,000 bp to
report the prediction of the models. At your own risk you can set this
threshold lower using the argument **min_length**

``` r
df_full_prediction <- plasmid_classification(path_input_file = my_path, full_output = TRUE, species = "Enterococcus faecium", min_length = 500)
```

    ## Warning in plasmid_classification(path_input_file = my_path, full_output
    ## = TRUE, : !!! Be aware that predictions for sequences < 1000 bp can be
    ## misleading !!!

## Help

**mlplasmids** only contains a single function called
**plasmid_classification**, you can learn more about arguments and
values required using the help page.

``` r
help('plasmid_classification')
```

# Advanced unix users installation/usage

Thanks to the fantastic work of @nickp60, mlplasmids can be installed
and used without the requirement to start R/Rstudio. This can be ideal
if you are intending to use mlplasmids on a high-performance-computing
facility or to integrate mlplasmids in your pipeline. Using your unix
terminal, you can first download the gitlab repo:

``` bash
git clone https://gitlab.com/sirarredondo/mlplasmids.git
cd mlplasmids/
```

There is a script called ‘run_mlplasmids.R’ (scripts/run_mlplasmids.R)
that will check whether mlplasmids is already present in your R
libraries or needs to be installed. You need to provide three arguments

-   1st argument: Input path of your fasta file
-   2nd argument: Output path in which mlplasmids will create and store
    the results (.tab)
-   3rd argument: Integer number ranging from 0 to 1 corresponding to
    the minimum probability threshold to classify a sequence as
    chromosome- or plasmid-derived.
-   4th argument: Character value. Species with plasmid classifiers
    available. You need to specify one of the following species:
    ‘Enterococcus faecium’,‘Klebsiella pneumoniae’, ‘Escherichia coli’,
    ‘Acinetobacter baumannii’ and ‘Enterococcus lactis’)

We can predict the complete genome of *E. faecium* AUS0004
(GCA_000250945.1)

``` bash
Rscript scripts/run_mlplasmids.R data/GCA_000250945.1_ASM25094v1_genomic.fna.gz examples/mlplasmids_results.tab 0.7 'Enterococcus faecium'
```

Now, we can check our prediction

``` bash
cat examples/mlplasmids_results.tab
```

# Troubleshooting

## Installation with R: Devtools

If installation fails you can try to install some required libraries in
your command-line terminal (bash):

``` bash
sudo apt-get install build-essential libcurl4-gnutls-dev libxml2-dev libssl-dev
```

## Installation with R: Biostrings error

In some new versions of R, you will get the following message:

*Biostrings not available for R (version)*

You can install Biostrings using BiocManager

``` r
if (!requireNamespace("BiocManager", quietly = TRUE))
    install.packages("BiocManager")

BiocManager::install("Biostrings")
```

Biostrings is required to calculate the pentamer frequencies (n=1024)
for each contig.

# Issues/Bugs

You can report any issues or bugs that you find while installing/running
**mlplasmids** using the [Issue
tracker](https://gitlab.com/sirarredondo/mlplasmids/issues)

# Citation

If you use **mlplasmids** package for your research please cite the
following publication:

Arredondo-Alonso et al., Microbial Genomics 2018;4 DOI
10.1099/mgen.0.000224

# Contributors and developers of mlplasmids

-   Julian Paganini
-   Jesse Kerkvliet
-   Alessia Carrara
-   Theodor Anton Ross
-   Sergio Arredondo-Alonso
-   Anita C. Schürch

# Future plans

You can contribute to the development of **mlplasmids** in different
ways:

-   If you have optimised a plasmid classifier and you would like to use
    mlplasmids implementation to make it available.
-   If you have a bunch of **complete genomes from a particular
    bacterial species** with short-read WGS data available and you would
    like to create a new plasmid model.

In any case, you can always send us an email:

-   Sergio Arredondo-Alonso - <s.a.alonso@medisin.uio.no>
-   Anita C. Schürch - <A.C.Schurch@umcutrecht.nl>
